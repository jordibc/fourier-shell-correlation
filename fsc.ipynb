{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "polyphonic-soccer",
   "metadata": {},
   "source": [
    "# Fourier Shell Correlation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "linear-daniel",
   "metadata": {},
   "outputs": [],
   "source": [
    "%run ../cryoem-common/cryoem_common.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dominant-landscape",
   "metadata": {},
   "outputs": [],
   "source": [
    "add_latex_commands()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "revised-poland",
   "metadata": {},
   "source": [
    "The purpose of this notebook is to explain the *Fourier Shell Correlation* (FSC) and present some code that computes it efficiently."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "urban-malta",
   "metadata": {},
   "source": [
    "For two volumes $V_1$ and $V_2$, which are functions that assign a \"Coulomb potential map\" (since electrons are scattered by Coulomb potential) to each point in space ($V_1, V_2: \\mathbb{R}^3 \\rightarrow \\mathbb{R}$), we define their FSC at a frequency $f$ as:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "immune-picture",
   "metadata": {},
   "source": [
    "$$\n",
    "\\FSC_{V_1,V_2}(f) =\n",
    "    \\frac{\\dotf{F_1}{F_2}}\n",
    "         {\\normf{F_1} \\normf{F_2}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "exposed-plumbing",
   "metadata": {},
   "source": [
    "where:\n",
    "\n",
    "* $F_1$ and $F_2$ are the (amplitude) *structure factors* corresponding to the volumes $V_1$ and $V_2$, that is, their Fourier transforms $F_1 = \\F{V_1}$, $F_2  = \\F{V_2}$.\n",
    "* $\\dotf{\\cdot}{\\cdot}$ is the inner product \"at frequency $f$\", $\\dotf{F}{G} = \\frac{1}{N} \\sum_{\\|\\vect{f'}\\| = f} F^*(\\vect{f'}) \\, G(\\vect{f'})$, where $N$ is the number of frequency voxels with absolute frequency $f$,  $N = \\sum_{\\|\\vect{f'}\\| = f} 1$, the sum being over all voxels $\\vect{f'}$ in frequency space that have a norm equal to $f$.\n",
    "* $\\normf{\\cdot}$ is the norm \"at frequency $f$\", $\\normf{F} = \\sqrt{\\dotf{F}{F}}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "functional-learning",
   "metadata": {},
   "source": [
    "Alternatively, we can see the inner product as that of vectors $\\ketf{F} = S_f \\, F$ where $S_f$ is a \"*shell at frequency* $f$\", a function such that $S_f(\\vect{f'})$ is a smooth approximation to $\\delta(\\|\\vect{f'}\\| - f)$.\n",
    "\n",
    "(Is the convolution of such a delta with a window function like a Hann window a good way to define $S_f$?)\n",
    "\n",
    "For example, we can use $S_f(\\vect{f'}) = e^{- \\frac{(\\|\\vect{f'}\\| - f)^2}{2 \\sigma^2}}$ for a certain $\\sigma$, that we can take to cover about 1 pixel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fantastic-immune",
   "metadata": {},
   "outputs": [],
   "source": [
    "def shell(f):\n",
    "    \"Return a shell of spatial frequencies, around frequency f\"\n",
    "    return exp(- (f_norm - f)**2 / (2 * f_width**2))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "noble-telling",
   "metadata": {},
   "source": [
    "This way, we would have the inner product and norm defined as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "necessary-princess",
   "metadata": {},
   "outputs": [],
   "source": [
    "def inner_prod(F, G, freq):\n",
    "    S = shell(freq)\n",
    "    SF = S * F\n",
    "    SG = S * G\n",
    "    return sum(conjugate(SF) * SG)\n",
    "\n",
    "\n",
    "def norm(F, freq):\n",
    "    return sqrt(inner_prod(F, F, freq))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "numeric-bankruptcy",
   "metadata": {},
   "source": [
    "And the FSC as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "endless-malpractice",
   "metadata": {},
   "outputs": [],
   "source": [
    "def FSC(freq):\n",
    "    return inner_prod(FV1, FV2, freq) / (norm(FV1, freq) * norm(FV2, freq))\n",
    "    # The values of FV1 and FV2 must correspond to the Fourier transforms of the two volumes V1 and V2."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "entire-seeker",
   "metadata": {},
   "source": [
    "Yet another way to see it is:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dated-slide",
   "metadata": {},
   "source": [
    "$$\n",
    "\\text{FSC}_{V_1,V_2}(f) =\n",
    "    \\frac{\\dot{V_1^{\\!f}}{V_2^{\\!f}}}\n",
    "         {\\norm{V_1^{\\!f}} \\norm{V_2^{\\!f}}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "mexican-shell",
   "metadata": {},
   "source": [
    "where $\\ket{V^{\\!f}}$ is the volume \"at frequency $f$\", $\\ket{V^{\\!f}} = \\IF{S_f \\, \\F{V}}$, and the sum of the inner product is over all spatial voxels, $\\sum_{\\vect{r}}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "spanish-ceremony",
   "metadata": {},
   "source": [
    "(Note that $\\dot{V_1}{V_2} = \\dot{\\F{V_1}}{\\F{V_2}}$, that is, the inner product of two functions is the same as the inner product of their Fourier transforms.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sexual-transcript",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ancient-domestic",
   "metadata": {},
   "source": [
    "### Reading Volumes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sharp-poker",
   "metadata": {},
   "source": [
    "The volumes we are going to consider are all defined on a grid of size $n \\times n \\times n$, where the voxel size is the same in every direction.\n",
    "\n",
    "We load our initial volume from two half-volumes stored in mrc files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "effective-letter",
   "metadata": {},
   "outputs": [],
   "source": [
    "vol1, voxel_n1, voxel_size1 = get_vol_and_voxel('emd_10418_half_map_1.map')\n",
    "vol2, voxel_n2, voxel_size2 = get_vol_and_voxel('emd_10418_half_map_2.map')\n",
    "\n",
    "assert voxel_size1 == voxel_size2 and voxel_n1 == voxel_n2\n",
    "voxel_size = voxel_size1\n",
    "voxel_n = voxel_n1\n",
    "\n",
    "vol = (vol1 + vol2) / 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "missing-abortion",
   "metadata": {},
   "source": [
    "We have a volume with the given dimensions and sample spacing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "illegal-stocks",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'Dimensions: {voxel_n}x{voxel_n}x{voxel_n}')\n",
    "print('Spacing: %g (A)' % voxel_size)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "atlantic-agency",
   "metadata": {},
   "source": [
    "### Plotting slices"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fatal-priest",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(vol1, title='Volume 1')\n",
    "plot(vol2, title='Volume 2')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "swiss-merchandise",
   "metadata": {},
   "source": [
    "## Selecting Frequencies"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "objective-engineering",
   "metadata": {},
   "source": [
    "We have to specify a bandwidth  to use when selecting a frequency, since we deal with a discrete approximation to an exact frequency. A good way to do it is to choose how wide it is going to be in frequency pixels (since we will want at least 1 pixel width, and possibly more to average more voxels when selecting a frequency!)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "actual-audience",
   "metadata": {},
   "outputs": [],
   "source": [
    "f_voxel_width = 1  # width (in voxels) of the shell used to select frequencies  \n",
    "\n",
    "f_width = f_voxel_width / (voxel_n * voxel_size)  # frequency width of the shell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "improving-drive",
   "metadata": {},
   "source": [
    "We define a volume in frequency space that at each frequency voxel $\\vect{f}$ contains its norm $f = \\norm{\\vect{f}}$, so we can reuse it for every call to `shell(f)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bigger-diabetes",
   "metadata": {},
   "outputs": [],
   "source": [
    "fx, fy, fz = fftnfreq(voxel_n, d=voxel_size)\n",
    "f_norm = sqrt(fx**2 + fy**2 + fz**2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cardiac-brass",
   "metadata": {},
   "source": [
    "### Frequency Shell"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "governing-transportation",
   "metadata": {},
   "source": [
    "With those values, we can see how the original `shell` function that we defined will look like in frequency space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "prospective-arbitration",
   "metadata": {},
   "outputs": [],
   "source": [
    "fplot(shell(f=0.2))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "appreciated-colony",
   "metadata": {},
   "source": [
    "### Volume At Selected Frequencies"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "chief-instrument",
   "metadata": {},
   "source": [
    "Let's look at how the volume would look like \"at selected frequencies\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "gorgeous-birth",
   "metadata": {},
   "outputs": [],
   "source": [
    "FV = fftn(vol)\n",
    "\n",
    "def vol_at_freq(freq):\n",
    "    S = shell(freq)\n",
    "    return real(ifftn(S * FV))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "variable-kruger",
   "metadata": {},
   "outputs": [],
   "source": [
    "for f in [0, 0.1, 0.2, 0.3]:  # A^-1\n",
    "    plot(vol_at_freq(f), title='Volume at frequency %g ($\\\\AA^{-1}$)' % f, add_colorbar=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sonic-private",
   "metadata": {},
   "source": [
    "## Fourier Shell Correlation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "confused-jamaica",
   "metadata": {},
   "source": [
    "Now we have all the ingredients to compute the FSC of the two volumes `vol1` and `vol2`, for which we will use the originally defined `FSC` function with the following structure factors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "assisted-rebate",
   "metadata": {},
   "outputs": [],
   "source": [
    "FV1 = fftn(vol1)\n",
    "FV2 = fftn(vol2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "japanese-running",
   "metadata": {},
   "source": [
    "Let's compute the FSC for all the possible frequencies, and do it in parallel (using `multiprocessing.Pool`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "internal-peeing",
   "metadata": {},
   "outputs": [],
   "source": [
    "fmin, fmax = 0, 1 / (2 * voxel_size)  # maximum frequency is Nyquist\n",
    "freqs = linspace(fmin, fmax, 40)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "surgical-governor",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "with Pool() as pool:\n",
    "    fscs = pool.map(FSC, freqs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "resident-emerald",
   "metadata": {},
   "source": [
    "And let's look at the results. The reference threshold that will define the resolution of the original volumes corresponds to that of an FSC = 1/7."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "owned-panic",
   "metadata": {},
   "outputs": [],
   "source": [
    "fsc_threshold = 1/7"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "southern-development",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(freqs, [real(fsc) for fsc in fscs], '.-', label='FSC')\n",
    "plt.xlabel('$1/d \\\\, (\\\\AA^{-1})$')\n",
    "plt.hlines(fsc_threshold, fmin, fmax, color='red', linestyle='dashed', label='resolution threshold')\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "tutorial-diary",
   "metadata": {},
   "outputs": [],
   "source": [
    "def find_resolution(fsc_threshold):\n",
    "    \"Return the resolution that corresponds to the given FSC threshold\"\n",
    "    return 1 / linear_interpolation(freqs, real(fscs), fsc_threshold)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "imposed-delaware",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The resolution of this volume, as computed by the FSC, is %.2f A.' % find_resolution(fsc_threshold=1/7))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "defined-christopher",
   "metadata": {},
   "source": [
    "### A Threshold of FSC=1/7"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "tribal-vinyl",
   "metadata": {},
   "source": [
    "If we take a threshold of $\\text{FSC} = 1/7$, what is the SNR of the full volume?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "mobile-stream",
   "metadata": {},
   "source": [
    "If we write the structure factors as the sum of the true value for the volume and a noise term, $F_i = S + N_i$ and simplify the notation by dropping the implied frequency $f$, so $\\FSC_{V_1,V_2}(f) \\rightarrow \\FSC$, and $\\dotf{}{} \\rightarrow \\dot{}{}$, etc., we then have:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cardiovascular-vintage",
   "metadata": {},
   "source": [
    "$$\n",
    "\\FSC =\n",
    "    \\frac{\\dot{F_1}{F_2}}\n",
    "         {\\norm{F_1} \\norm{F_2}}\n",
    "    =\n",
    "    \\frac{\\dot{S + N_1}{S + N_2}}\n",
    "         {\\norm{S + N_1} \\norm{S + N_2} }\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "north-liabilities",
   "metadata": {},
   "source": [
    "<span title=\"doubtful\" style=\"color:red\">**NOTE**: All these things about expected values seems to me to be *wrong*! The expected value is a lineal operator, and the expected value of the product is the product of the expected values if the terms are independent, but that's it. The expected value of the square of something is not the square of the expected value, and neither for the square root or the inverse. So **everything below here is just a draft of me trying to figure things out**.</span>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "legal-arthritis",
   "metadata": {},
   "source": [
    "Using the linearity of the inner product to expand both the numerator and also the norms in the denominator, we find that its expected value will be:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "intended-studio",
   "metadata": {},
   "source": [
    "$$\n",
    "\\E{\\FSC} =\n",
    "    \\frac{\\norm{S}^2}\n",
    "         {\\E{\\norm{S + N_1}^2}} =\n",
    "    \\frac{\\norm{S}^2}\n",
    "         {\\norm{S}^2 + \\E{\\norm{N_1}^2}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "mighty-insulin",
   "metadata": {},
   "source": [
    "since the noises are uncorrelated between them, $\\E{\\dot{N_1}{N_2}} = \\E{\\bra{N_1}} \\, \\E{\\ket{N_2}}$, and uncorrelated with the true signal, $\\E{\\dot{S}{N_i}} = \\E{\\bra{S}} \\, \\E{\\ket{N_i}}$, and have 0 expected value, $\\E{\\ket{N_i}} = \\ket{0}$, and the expected values of $\\norm{S + N_1}$ and $\\norm{S + N_2}$ are the same."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "coupled-quick",
   "metadata": {},
   "source": [
    "Rearranging terms, we have:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "vietnamese-royal",
   "metadata": {},
   "source": [
    "$$\n",
    "\\frac{\\norm{S}^2}{\\E{\\norm{N_1}^2}} = \\frac{\\E{\\FSC}}{1 - \\E{\\FSC}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "adapted-avatar",
   "metadata": {},
   "source": [
    "When we join the two half-maps and look at the full volume $V = \\frac{V_1 + V_2}{2}$, we have twice the number of samples compared to each of the half-maps, and thus its expected noise will be $\\E{\\norm{N}} = \\E{\\norm{N_1}} / \\sqrt{2}$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "digital-fleece",
   "metadata": {},
   "source": [
    "Defining the [signal-to-noise ratio](https://en.wikipedia.org/wiki/Signal-to-noise_ratio) as $\\SNR = \\norm{S}^2 / \\E{\\norm{N}^2} = 2 \\norm{S}^2 / \\E{\\norm{N_1}^2}$, we have:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "intended-address",
   "metadata": {},
   "source": [
    "$$\n",
    "\\SNR = \\frac{2 \\E{\\FSC}}{1 - \\E{\\FSC}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "configured-azerbaijan",
   "metadata": {},
   "source": [
    "We use the experimentally found value of $\\FSC$ as an estimation for the expected value $\\E{\\FSC}$. Thus, **for FSC = 1/7, we have an estimated value of SNR = 1/3**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "interim-conditioning",
   "metadata": {},
   "source": [
    "Why do we use $\\FSC = 1/7$ then? If we compute the FSC of the full volume with the true volume, $\\FSC'$, and see at which frequency it goes to 1/2, that would be at the frequency where the FSC of the two half-volumes equals 1/7. Let's see it:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "opponent-brick",
   "metadata": {},
   "source": [
    "$$\n",
    "\\FSC' = \\frac{\\dot{S}{S + N}}{\\norm{S} \\norm{S + N}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "unknown-destruction",
   "metadata": {},
   "source": [
    "and"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "opened-impression",
   "metadata": {},
   "source": [
    "$$\n",
    "\\E{\\FSC'}\n",
    "      = \\frac{\\norm{S}}{\\E{\\norm{S + N}}}\n",
    "      = \\frac{\\norm{S}}{\\sqrt{\\norm{S}^2 + \\E{\\norm{N}^2}}}\n",
    "      = \\sqrt{\\frac{\\SNR}{\\SNR + 1}}\n",
    "      = \\sqrt{\\frac{2 \\E{\\FSC}}{1 + \\E{\\FSC}}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "juvenile-wrong",
   "metadata": {},
   "source": [
    "Indeed, for $\\E{\\FSC} = 1/7$, one has $\\E{\\FSC'} = 1/2$. So what? The argument made at [Rosenthal and Henderson](https://pubmed.ncbi.nlm.nih.gov/14568533/) (where they write $C_\\text{ref}$ instead of $\\E{\\FSC'}$) is that it corresponds to the equivalent figure-of-merit $m$ used in X-ray crystallography, and there they use that threshold ($m = 0.5$, which also corresponds to a phase error of $60^\\circ$)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acoustic-beaver",
   "metadata": {},
   "source": [
    "### SNR From Volumes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dangerous-setting",
   "metadata": {},
   "source": [
    "These are some thought about how we should actually compute the SNR, instead of using the FSC."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "national-junction",
   "metadata": {},
   "source": [
    "We will be using $V = \\frac{V_1 + V_2}{2}$ and $N = \\frac{V_1 - V_2}{2}$. Given that $V_i = S + N_i$, we have:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dated-stevens",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{eqnarray}\n",
    "V & = & \\frac{(S + N_1) + (S + N_2)}{2} = S + \\frac{N_1 + N_2}{2} =: S + N_+ \\\\\n",
    "N & = & \\frac{(S + N_1) - (S + N_2)}{2} = \\frac{N_1 - N_2}{2} =: N_-\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "graphic-looking",
   "metadata": {},
   "source": [
    "$$\n",
    "\\begin{eqnarray}\n",
    "\\frac{\\norm{V}^2}{\\norm{N}^2} & = & \\frac{\\norm{S}^2 + 2 \\dot{S}{N_+} + \\norm{N_+}^2}{\\norm{N_-}^2} \\\\\n",
    "    & = & \\frac{\\norm{S}^2}{\\norm{N_-}^2} + \\frac{2}{\\norm{N_-}^2} \\dot{S}{N_+} + \\frac{\\norm{N_+}^2}{\\norm{N_-}^2}\n",
    "\\end{eqnarray}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "numerous-thomson",
   "metadata": {},
   "source": [
    "<span title=\"doubtful\" style=\"color:red\">Now I can easily convince myself that the expected value of the second term is 0 (since $\\ket{N_+}$ is going to point to all directions with equal probability), and the expected value of the third term is 1 (of which I am actually not so sure).</span> *If those assumptions are valid*, we have the following relation for expected values:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "realistic-illustration",
   "metadata": {},
   "source": [
    "$$\n",
    "\\E{\\frac{\\norm{V}^2}{\\norm{N}^2}} = \\SNR + 1\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "postal-victorian",
   "metadata": {},
   "source": [
    "and"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "completed-fourth",
   "metadata": {},
   "source": [
    "$$\n",
    "\\SNR = \\E{\\frac{\\norm{V}^2}{\\norm{N}^2}} - 1 = \\E{\\frac{\\norm{V}^2 - \\norm{N}^2}{\\norm{N}^2}}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "facial-titanium",
   "metadata": {},
   "source": [
    "which would seem quite straightforward, with the numerator an estimation of $\\norm{S}^2$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fuzzy-cambridge",
   "metadata": {},
   "source": [
    "In any case, if this is correct, then, since $V^{\\!f} = \\IF{\\frac{S_f F_1 + S_f F_2}{2}}$ and the inner product is the same in frequency space and real space, if we take as an estimation of the SNR at a certain frequency $\\hat{\\SNR}(f)$:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "downtown-apartment",
   "metadata": {},
   "source": [
    "$$\n",
    "\\hat{\\SNR}(f) =\n",
    "  \\frac{\\norm{V^{\\!f}}^2}{\\norm{N^{\\!f}}^2} - 1 =\n",
    "  \\frac{\\normf{F_1 + F_2}^2}{\\normf{F_1 - F_2}^2} - 1 =\n",
    "  \\frac{4 \\dotf{F_1}{F_2}}{\\normf{F_1 - F_2}^2} =\n",
    "  \\frac{\\dotf{F_1}{F_2}}{\\normf{\\frac{F_1 - F_2}{2}}^2} =\n",
    "  \\frac{\\dotf{F_1}{F_2}}{\\normf{N}^2}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "allied-mississippi",
   "metadata": {},
   "source": [
    "(Note that $\\Re \\dot{F_1}{F_2} = \\dot{F_1}{F_2}$, since $\\dot{F_1}{F_2} = \\dot{V_1}{V_2}$, and $V_1$ and $V_2$ are real.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "informal-source",
   "metadata": {},
   "source": [
    "(For comparison, remember that $\\FSC(f) = \\frac{\\dotf{F_1}{F_2}}{\\normf{F_1} \\normf{F_2}}$.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aggregate-suspension",
   "metadata": {},
   "outputs": [],
   "source": [
    "def SNR_slow(freq):\n",
    "    return real(inner_prod(F1, F2, freq)) / norm((F1 - F2)/2, freq)**2\n",
    "    # The values of F1 and F2 must correspond to the Fourier transforms of the two volumes V1 and V2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "checked-weight",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A faster way to compute the SNR.\n",
    "V = (vol1 + vol2) / 2\n",
    "N = (vol1 - vol2) / 2\n",
    "FV = fftn(V)\n",
    "FN = fftn(N)\n",
    "def SNR(freq):\n",
    "    S = shell(freq)\n",
    "    SFV = abs(S * FV)\n",
    "    SFN = abs(S * FN)\n",
    "    return sum(SFV * SFV) / sum(SFN * SFN) - 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "standard-intake",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "with Pool() as pool:\n",
    "    snrs = pool.map(SNR, freqs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "large-wright",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(freqs, abs(snrs), '.-', label='SNR')\n",
    "plt.yscale('log')\n",
    "plt.xlabel('$1/d \\\\, (\\\\AA^{-1})$')\n",
    "plt.hlines(1, fmin, fmax, color='green', linestyle='dashed', label='SNR = 1')\n",
    "plt.hlines(1/3, fmin, fmax, color='red', linestyle='dashed', label='SNR = 1/3')\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "crucial-insider",
   "metadata": {},
   "outputs": [],
   "source": [
    "def find_resolution_snr(snr_threshold):\n",
    "    \"Return the resolution that corresponds to the given SNR threshold\"\n",
    "    return 1 / linear_interpolation(freqs, snrs, snr_threshold)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "exclusive-variation",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The resolution of this volume at SNR=1   is %.2f A.' % find_resolution_snr(snr_threshold=1))\n",
    "print('The resolution of this volume at SNR=1/3 is %.2f A.' % find_resolution_snr(snr_threshold=1/3))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
